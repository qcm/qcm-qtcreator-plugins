## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):error("Please execute 'export QTC_SOURCE=<path-to-qtcreator-src>' and 'export QTC_BUILD=<path-to-qtcreator-build>' before running qmake")

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):error("Please execute 'export QTC_SOURCE=<path-to-qtcreator-src>' and 'export QTC_BUILD=<path-to-qtcreator-build>' before running qmake")
