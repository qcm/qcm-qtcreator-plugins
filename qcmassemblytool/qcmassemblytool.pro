TARGET = QCMAssemblyTool
TEMPLATE = lib

DEFINES += QCMASSEMBLYTOOL_LIBRARY

# QCMAssemblyTool files

SOURCES += qcmassemblytoolplugin.cpp\
           qcmassemblytoolview.cpp

HEADERS += qcmassemblytoolplugin.h\
           qcmassemblytool_global.h\
           qcmassemblytoolconstants.h\
           qcmassemblytoolview.h

RESOURCES += qcmassemblytool.qrc

include(../check-qtcreator.pri)

PROVIDER = LiveBlue

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)
include($$QTCREATOR_SOURCES/src/plugins/coreplugin/coreplugin.pri)

LIBS += -L$$IDE_PLUGIN_PATH/Nokia

INSTALLS =
