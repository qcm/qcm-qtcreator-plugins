/***************************************************************************
 *   Copyright (C) 2010 by Sandro Andrade <sandroandrade@kde.org>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "qcmassemblytoolplugin.h"
#include "qcmassemblytoolconstants.h"

#include <QtPlugin>
#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>

#include <coreplugin/imode.h>
#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>

#include "qcmassemblytoolview.h"

namespace QCMAssemblyTool {
namespace Internal {

class AssemblyToolMode : public Core::IMode
{
public:
    AssemblyToolMode()
    {
        setWidget(new QCMAssemblyToolView);
	setContext(Core::Context("AssemblyTool.MainView"));
	setDisplayName(tr("QCM Assembly Tool"));
	setIcon(QIcon(":/qcmproject/images/qcm_wizard.png"));
	setPriority(0);
	setId(Constants::MODE_ID);
	setType(Constants::MODE_TYPE);
	setContextHelpId(QString());
    }
};

QCMAssemblyToolPlugin::QCMAssemblyToolPlugin()
{
    // Create your members
}

QCMAssemblyToolPlugin::~QCMAssemblyToolPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool QCMAssemblyToolPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // connect to other plugins' signals
    // "In the initialize method, a plugin can be sure that the plugins it
    //  depends on have initialized their members."

    Q_UNUSED(arguments)
    Q_UNUSED(errorString)
    Core::ActionManager *am = Core::ICore::instance()->actionManager();

    QAction *action = new QAction(tr("QCMAssemblyTool action"), this);
    Core::Command *cmd = am->registerAction(action, Constants::ACTION_ID,
                         Core::Context(Core::Constants::C_GLOBAL));
    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Alt+Meta+A")));
    connect(action, SIGNAL(triggered()), this, SLOT(triggerAction()));

    Core::ActionContainer *menu = am->createMenu(Constants::MENU_ID);
    menu->menu()->setTitle(tr("QCMAssemblyTool"));
    menu->addAction(cmd);
    am->actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);

    Core::Context context("AssemblyTool.MainView");
    Core::IMode *assemblyToolMode = new AssemblyToolMode;
    addAutoReleasedObject(assemblyToolMode);
    
    return true;
}

void QCMAssemblyToolPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // "In the extensionsInitialized method, a plugin can be sure that all
    //  plugins that depend on it are completely initialized."
}

ExtensionSystem::IPlugin::ShutdownFlag QCMAssemblyToolPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void QCMAssemblyToolPlugin::triggerAction()
{
    QMessageBox::information(Core::ICore::instance()->mainWindow(),
                             tr("Action triggered"),
                             tr("This is an action from QCMAssemblyTool."));
}

}
}

Q_EXPORT_PLUGIN2(QCMAssemblyTool, QCMAssemblyTool::Internal::QCMAssemblyToolPlugin)
