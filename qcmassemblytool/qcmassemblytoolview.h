/***************************************************************************
 *   Copyright (C) 2010 by Sandro Andrade <sandroandrade@kde.org>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef QCMASSEMBLYTOOLVIEW_H
#define QCMASSEMBLYTOOLVIEW_H

#include "qcmassemblytool_global.h"

#include <QGraphicsView>

namespace QCMAssemblyTool {
namespace Internal {

class QCMAssemblyToolView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit QCMAssemblyToolView(QWidget *parent = 0);
    ~QCMAssemblyToolView();

};

}
}

#endif // QCMASSEMBLYTOOLVIEW_H

