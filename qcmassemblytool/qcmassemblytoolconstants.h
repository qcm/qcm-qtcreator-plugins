/***************************************************************************
 *   Copyright (C) 2010 by Sandro Andrade <sandroandrade@kde.org>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef QCMASSEMBLYTOOLCONSTANTS_H
#define QCMASSEMBLYTOOLCONSTANTS_H

namespace QCMAssemblyTool {
namespace Constants {

    const char * const ACTION_ID = "QCMAssemblyTool.Action";
    const char * const MENU_ID = "QCMAssemblyTool.Menu";
    const char * const MODE_ID = "AssemblyTool.AssemblyToolMode";
    const char * const MODE_TYPE = "AssemblyTool.AssemblyToolMode";

} // namespace QCMAssemblyTool
} // namespace Constants

#endif // QCMASSEMBLYTOOLCONSTANTS_H

