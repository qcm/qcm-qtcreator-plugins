/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "%ProjectName%.h"

#include <QtCore/QDebug>

#include <qcm/qcmobjecthome.h>

%ProjectName%::%ProjectName%()
{
    qDebug() << "New %ProjectName% instance created !";
}

%ProjectName%::~%ProjectName%()
{
    qDebug() << "%ProjectName% instance destroyed !";
}

void %ProjectName%::go()
{
    emit sayMessage ("QCM");
}

Q_EXPORT_PLUGIN2(%ProjectName%, QCMObjectHome<%ProjectName%>)

