TEMPLATE = subdirs

include (../check-qtcreator.pri)

wizard.path = $$IDE_BUILD_TREE/share/qtcreator/templates/wizards/qcm-component/
wizard.files = wizard.xml component.pro *.h *.cpp qcm_wizard.png

INSTALLS += wizard
