TEMPLATE = lib
CONFIG += plugin
TARGET = $$qtLibraryTarget(%ProjectName%)
INCLUDEPATH += $$(QCM_ROOT)/
LIBS += -L$$(QCM_BUILD)/qcm

HEADERS += %ProjectName%.h
SOURCES += %ProjectName%.cpp

target.path = $$(QCM_INSTALL)/lib/
INSTALLS += target
